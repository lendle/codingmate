/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author lendle
 */
public class CodingMateTest extends TestCase{
    private CodingMate codingMate=null;

    @Override
    protected void tearDown() throws Exception {
        super.tearDown(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp(); //To change body of generated methods, choose Tools | Templates.
        codingMate=new CodingMate();
    }
    
    @Test
    public void testGetCategories(){
        List<String> list=codingMate.getCategories();
        assertNotNull(list);
    }
    
    @Test
    public void testGetQuestions(){
        List<Question> list=codingMate.getQuestions("dummy");
    }
}
