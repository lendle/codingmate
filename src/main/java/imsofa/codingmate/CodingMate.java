/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class CodingMate {

    private static final String DATA_DIR = "data";

    public List<String> getCategories() {
        File dataDir = new File(DATA_DIR);
        return Arrays.asList(dataDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                //System.out.println(dir.getAbsolutePath()+":"+name);
                return name.startsWith(".") == false;
            }
        }));
    }

    public Question getQuestion(String category, String name) {
        try {
            Question question = new Question();
            File dataDir = new File(DATA_DIR);
            File categoryDir = new File(dataDir, category);
            File questionDir = new File(categoryDir, name);
            //System.out.println(questionDir.getAbsolutePath());
            if (!questionDir.exists() || !questionDir.isDirectory()) {
                return null;
            }
            
            Properties props = new Properties();
            try (Reader reader = new InputStreamReader(new FileInputStream(new File(questionDir, "info.properties")), "utf-8")) {
                props.load(reader);
            }
            question.setName(name);
            Scaffolding s = new Scaffolding();
            s.setMethodSignature(FileUtils.readFileToString(new File(questionDir, "signature.txt"), "utf-8"));
            s.setPrevContent(FileUtils.readFileToString(new File(questionDir, "prev.txt"), "utf-8"));
            s.setNextContent(FileUtils.readFileToString(new File(questionDir, "next.txt"), "utf-8"));
            question.setDescription(props.getProperty("description"));
            question.setScaffolding(s);
            question.setValidationCode(FileUtils.readFileToString(new File(questionDir, "validation.txt"), "utf-8"));
            //process hints
            File hintsDir=new File(questionDir, "hints");
            if(hintsDir.exists() && hintsDir.isDirectory()){
                List<Hint> hints=new ArrayList<Hint>();
                question.setHints(hints);
                File urlsFile=new File(hintsDir, "urls.txt");
                if(urlsFile.exists() && urlsFile.isFile()){
                    List<String> urls=FileUtils.readLines(urlsFile, "utf-8");
                    for(String url : urls){
                        URLHint hint=new URLHint();
                        hint.setTitle(url);
                        hint.setUrl(new URL(url));
                        hints.add(hint);
                    }
                }
                for(File file : hintsDir.listFiles(new FilenameFilter(){
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.startsWith(".")==false && name.equals("urls.txt")==false;
                    }
                })){
                    TextHint hint=new TextHint();
                    hint.setTitle(file.getName());
                    hint.setText(FileUtils.readFileToString(file, "utf-8"));
                    hints.add(hint);
                }
            }
            
            return question;
        } catch (IOException ex) {
            Logger.getLogger(CodingMate.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public List<Question> getQuestions(String category) {
        File dataDir = new File(DATA_DIR);
        File categoryDir = new File(dataDir, category);

        List<Question> ret = new ArrayList<>();
        for (String questionName : categoryDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith(".") == false;
            }
        })) {
            //System.out.println(questionName+":"+category);
            ret.add(this.getQuestion(category, questionName));
        }

        return ret;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(new File(".").getCanonicalPath());
    }
}
