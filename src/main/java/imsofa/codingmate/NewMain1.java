/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

/**
 *
 * @author lendle
 */
public class NewMain1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Question question=new Question();
        Scaffolding s=new Scaffolding();
        s.setMethodSignature("int add(int x, int y)");
        question.setScaffolding(s);
        question.setValidationCode("3==add(1, 2) && 5==add(2, 3) && 3==add(5, -2)");
        QuestionSolver solver=new QuestionSolver();
        solver.setQuestion(question);
        System.out.println(solver.test("return x-y;"));
    }
    
}
