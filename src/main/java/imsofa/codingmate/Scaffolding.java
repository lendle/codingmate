/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

/**
 *
 * @author lendle
 */
public class Scaffolding {
    private String methodSignature;
    private String prevContent;
    private String nextContent;

    public String getMethodSignature() {
        return methodSignature;
    }

    public void setMethodSignature(String methodSignature) {
        this.methodSignature = methodSignature;
    }

    public String getPrevContent() {
        return prevContent;
    }

    public void setPrevContent(String prevContent) {
        this.prevContent = prevContent;
    }

    public String getNextContent() {
        return nextContent;
    }

    public void setNextContent(String nextContent) {
        this.nextContent = nextContent;
    }
    
    
}
