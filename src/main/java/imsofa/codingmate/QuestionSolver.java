/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class QuestionSolver {
    private Question question=null;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
    
    public boolean test(String result){
        try {
            StringBuilder sb=new StringBuilder();
            Scaffolding scaffolding=question.getScaffolding();
            sb.append(scaffolding.getMethodSignature()).append("{").append("\n");
            if(scaffolding.getPrevContent()!=null){
                sb.append(scaffolding.getPrevContent()).append("\n");
            }
            sb.append(result);
            if(scaffolding.getNextContent()!=null){
                sb.append(scaffolding.getNextContent()).append("\n");
            }
            sb.append("}");
            sb.append(question.getValidationCode());
            bsh.Interpreter interpreter=new bsh.Interpreter();
            Boolean boolResult=(Boolean) interpreter.eval(sb.toString());
            return boolResult.booleanValue();
        } catch (Throwable ex) {
            Logger.getLogger(QuestionSolver.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
