/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

import java.net.URL;

/**
 *
 * @author lendle
 */
public class URLHint extends Hint{
    private URL url=null;

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
    
}
