/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author lendle
 */
public class Question {
    private String name;
    private String category;
    private Difficulty difficulty;
    private String description;
    private String validationCode;
    private Scaffolding scaffolding;
    private List<Hint> hints=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
    public List<Hint> getHints() {
        return hints;
    }

    public void setHints(List<Hint> hints) {
        this.hints = hints;
    }

    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public Scaffolding getScaffolding() {
        return scaffolding;
    }

    public void setScaffolding(Scaffolding scaffolding) {
        this.scaffolding = scaffolding;
    }
    
    
}
