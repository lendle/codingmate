/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.codingmate;

/**
 *
 * @author lendle
 */
public class NewMain2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        QuestionDialog dlg=new QuestionDialog(null);
        Question question=new Question();
        question.setName("adder");
        Scaffolding s=new Scaffolding();
        s.setMethodSignature("int add(int x, int y)");
        question.setDescription("加法設計");
        question.setScaffolding(s);
        question.setValidationCode("3==add(1, 2) && 5==add(2, 3) && 3==add(5, -2)");
        dlg.setQuestion(question);
        dlg.setVisible(true);
    }
    
}
