import java.util.*;
import java.text.*;

Date d=new Date();
Calendar c=Calendar.getInstance();
c.setTime(d);
DateFormatSymbols symbol=new DateFormatSymbols();
String str=symbol.getShortMonths()[c.get(Calendar.MONTH)];
return str.equals(getCurrentMonthName());
